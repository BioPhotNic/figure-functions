function makeAnnotations(xin,yin,textin,xstep,ystep,color)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% title : makeAnnotations.m
% Author : Nicolas Desjardins-Lecavalier
% Edition date : 1er juin 2021
% Description : puts some text on a graph without overlay
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if not(iscolumn(xin))
    xin = xin';
end
if not(iscolumn(yin))
    yin = yin';
end

yin(isnan(yin))=0;

% first label localization
xout(xin<=0,1) = xin(xin<=0)-xstep;
xout(xin>0,1) = xin(xin>0)+xstep;

yout(yin<=0,1) = yin(yin<=0)-ystep;
yout(yin>0,1) = yin(yin>0)+ystep;

diffy = abs(yout-yout');
diffx = abs(xout-xout');

group = zeros(size(yin));

% define groups by proximity
tooClose = diffy<=ystep & diffx<=xstep;
tooCloseIdx = sum(sum(tooClose));

if tooCloseIdx>size(diffy,1)
    for idx = 1:size(diffy,1)
        closeIdx = find(tooClose(:,idx));
        if length(closeIdx)>1
            if sum(group(closeIdx))>0
                group(closeIdx) = max(group(closeIdx));
            else 
                group(closeIdx) = max(group)+1;
            end                
        end
    end
    for thisGr = unique(group)'
        if thisGr>0
            iGr = group == thisGr;
            meanY = mean(yout(iGr));
            newY = [0:sum(iGr)-1]*ystep+meanY;
            yout(iGr) = newY;
        end
    end
end

x = [xin xout];
y = [yin yout];

% for idx = 1:length(xin)
    line(x',y','color','k')
% end
text(xout(xin>0),yout(xin>0),textin(xin>0),'Color',color)
text(xout(xin<=0),yout(xin<=0),textin(xin<=0),'HorizontalAlignment','right','Color',color)