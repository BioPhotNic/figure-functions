function myViolinPlot(data,groups,colors,varargin)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author : Nicolas Desjardins-Lecavalier
% Edition date : 2022-02-01
% Inputs :
%   - data : numerical vector ordered as groups
%   - groups : list of the groups corresponding to the data
%   - colors : 3xN matrix with the RGB triplet for every group
%   - 'plotDots' = true or false
%   - 'dotSize' = area in mm^2 (50 by default)
%   - 'linewidth' = in mm
%   - 'vertical' = true or false (false by default)
%   - 'boxPlot' = true or false (true by default) The box frames the two
%   central quartiles (Q1-Q2, Q2-Q3), the midle bar is the median (Q2) and
%   the end of the moustaches are percentile 5 and percentile 95
%   -'legend' = cell array
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    plotDotIdx = find(strcmp(varargin,'plotDot'));
    dotSizeIdx = find(strcmp(varargin,'dotSize'));
    linewidthIdx = find(strcmp(varargin,'linewidth'));
    verticalIdx = find(strcmp(varargin,'vertical'));
    nIdx = find(strcmp(varargin,'showN'));
    legendIdx = find(strcmp(varargin,'legend'));
    boxPlotIdx = find(strcmp(varargin,'boxPlot'));
    
    if isempty(plotDotIdx)
        plotDot = true;
    else
        plotDot = varargin{plotDotIdx+1};
    end
    
    if isempty(verticalIdx)
        vertical = false;
    else
        vertical = varargin{verticalIdx+1};
    end
    
    if isempty(dotSizeIdx)
        sz = 5;
    else
        sz = varargin{dotSizeIdx+1}*(1/0.352778)^2;
    end
    
    if isempty(linewidthIdx)
        linewidth = 1;
    else
        linewidth = varargin{linewidthIdx+1};
    end
    
    if isempty(nIdx)
        showN = true;
    else
        showN = varargin{nIdx+1};
    end
    
    if isempty(legendIdx)
        showLegend = false;
    else
        showLegend = true;
        theLegend = varargin{legendIdx+1};
    end
    
    if isempty(boxPlotIdx)
        boxPlot = true;
    else
        boxPlot = varargin{boxPlotIdx+1};
    end

%%

uniqueGroups = unique(groups,'stable');
dotDensity = length(data)/length(uniqueGroups);
thick = 2;
boxThick = 1;
boxHeight = 0.07;

for idx = 1:numel(uniqueGroups);
    
    thisColor = colors(idx,:);
    darkColor = 0.7*thisColor;
    g = uniqueGroups{idx};
    
    
    d = data(strcmp(groups,g));
    d(isnan(d)) = [];
    n = length(d);
    % percentiles
    C96(idx) = prctile(d,95.5);
    C4(idx) = prctile(d,4.5);
    
    x = sort(unique([linspace(min(d),max(d),1e4)]));
    [h] = ksdensity(d,x);
    h = 0.4*h/max(h);
    
    if plotDot
        [Ndots,~] = histcounts(d,round(n*std(d,'omitnan')/(C96(idx)-C4(idx))));
        facnorm = 0.8*max(h)/max(Ndots-1);
        y = [];
        for idxh=1:length(Ndots)
            y = [y, 2*(facnorm*((1:Ndots(idxh))-1)-mean(facnorm*((1:Ndots(idxh))-1)))];
        end
    end
    
    [mx,imx] = max(h + idx);
    hold on
    if vertical
       %plot contours
        plot(-h + idx,x,'LineWidth',thick,'color',darkColor,'linewidth',linewidth)
        plot(h + idx,x,'LineWidth',thick,'color',darkColor,'linewidth',linewidth)
        fill([-h,h(end:-1:1)]+ idx,[x x(end:-1:1)],thisColor,'edgecolor','none')
        alpha(.5)
        if showN
            text(mx+0.1,x(imx),['n = ' num2str(n)],'HorizontalAlignment','center')
        end
        % plot the data points
        if plotDot
            dots(idx)=scatter(y+idx,sort(d),sz,thisColor,'filled');
            alpha(.5)
        end
        %boxplot
        if boxPlot
            myBoxPlot(d,darkColor,boxHeight,1.5*thick,idx,vertical)
        end
        %graph axes and ticks
        ylim([min(C4) max(C96)])
        xticks([1:numel(uniqueGroups)])
        xticklabels(uniqueGroups)
    else
        %plot contours
        plot(x, h + idx,'LineWidth',thick,'color',thisColor,'linewidth',linewidth)
        plot(x,-h + idx,'LineWidth',thick,'color',thisColor,'linewidth',linewidth)
        if showN
            text(x(imx),mx+0.1,['n = ' num2str(n)],'HorizontalAlignment','center')
        end
        % plot the data points
        if plotDot
            dots(idx)=scatter(sort(d),y+idx,sz,thisColor,'filled');
            alpha(.5)
        end
        %boxplot
        if boxPlot
            myBoxPlot(d,darkColor,boxHeight,boxThick,idx,vertical)
        end
        %graph axes and ticks
        xlim([min(C4) max(C96)])
        yticks([1:numel(uniqueGroups)])
        yticklabels(uniqueGroups)
    end
end

if showLegend
    for iLeg = 1:numel(theLegend)
        thisColor = colors(iLeg,:);
        theseDots(iLeg) = scatter([],[],sz,thisColor,'filled');
        alpha(.5)
    end
    legend(theseDots,theLegend)
end