function [meanData,semData,uniqueGroups] = myMeanPlot(data,groups,colors,varargin)

% if numel(varargin)>0
    dotSizeIdx = find(strcmp(varargin,'dotSize'));
    verticalIdx = find(strcmp(varargin,'vertical'));
    nIdx = find(strcmp(varargin,'showN'));
    errorIdx = find(strcmp(varargin,'errorBar'));
    
    if isempty(verticalIdx)
        vertical = false;
    else
        vertical = varargin{verticalIdx+1};
    end
    
    if isempty(dotSizeIdx)
        sz = 5;
    else
        sz = varargin{dotSizeIdx+1};
    end
    
    if isempty(nIdx)
        showN = true;
    else
        showN = varargin{nIdx+1};
    end
    
    if isempty(errorIdx)
        errorBar = 'sem';
    else
        errorBar = varargin{errorIdx+1};
    end
% end

%%

uniqueGroups = unique(groups,'stable');
thick = 2;

for idx = 1:numel(uniqueGroups);
    
    thisColor = colors(idx,:);
    darkColor = 0.7*thisColor;
    g = uniqueGroups{idx};
    
    
    d = data(strcmp(groups,g));
    n = length(d);
    
    meanData(idx) = mean(d,'omitnan');
    stdData(idx) = std(d,'omitnan');
    semData(idx) = stdData(idx)/sqrt(n);
    
    switch errorBar
        case 'std'
            err = stdData(idx);
        case 'sem'
            err = semData(idx);
    end 

    hold on
    %plot
    errorbar(meanData(idx),idx,err,'horizontal','o','MarkerSize',sz,...
    'MarkerEdgeColor',thisColor,'MarkerFaceColor',thisColor,'Color',darkColor)
    if showN
        text(meanData(idx),idx+0.1,['n = ' num2str(n)],'HorizontalAlignment','center')
    end
    %graph axes and ticks
    ylim([0,numel(uniqueGroups)+1])
    yticks([1:numel(uniqueGroups)])
    yticklabels(uniqueGroups)
end