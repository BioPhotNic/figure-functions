function myDistributionComparator(data,groups,nComp,colors,plotDots,varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Title :           myDistributionComparator.m
% Author :          Nicolas Desjardins-L.
% Edition date :    24 march 2021
% Description :     plots distributions one on top of the other. Represents
%                   the data with dots and the distribution with a solid line.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inputs :
%   - data : numerical vector ordered as groups
%   - groups : list of the groups corresponding to the data
%   - nComp : how many distributions overlays
%   - colors : 3xN matrix with the RGB triplet for every group
%   - plotDots : true or false
%   - 'dotSize' = area in mm^2 (50 by default)
%   - 'linewidth' = in mm
%   - 'stars' : array of significant stars
%   - 'foldChange'
%   - 'log2FoldChange' : numeric array
%   - 'p' : numeric array for stars
%   - 'violin'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin == 0
    %% test
run('..\data6juilletPhi.m')
nComp = 3;
colors = [1 0 0; 1 0 0; 1 0 0; 0 0 1; 0 0 1; 0 0 1];
end
if numel(varargin)>0
    dotSizeIdx = find(strcmp(varargin,'dotSize'));
    linewidthIdx = find(strcmp(varargin,'linewidth'));
    starIdx = find(strcmp(varargin,'stars'));
    foldChangeIdx = find(strcmp(varargin,'foldChange'));
    log2FoldChangeIdx = find(strcmp(varargin,'log2FoldChange'));
    pIdx = find(strcmp(varargin,'p'));
    violinIdx = find(strcmp(varargin,'violin'));
else
    dotSizeIdx = [];
    linewidthIdx = [];
    starIdx = [];
    foldChangeIdx = [];
    log2FoldChangeIdx = [];
    pIdx = [];
    violinIdx = [];
end    
    if isempty(dotSizeIdx)
        sz = [];
    else
        sz = varargin{dotSizeIdx+1}*(1/0.352778)^2;
    end
    
    
    if isempty(linewidthIdx)
        thick = 2;
    else
        thick = varargin{linewidthIdx}*1/0.352778;
    end
    
    if isempty(starIdx)
        stars = {};
    else
        stars = varargin{starIdx+1};
    end
    
    if isempty(foldChangeIdx)
        FCnum = [];
    else
        FCnum = varargin{foldChangeIdx+1};
    end
    
    if isempty(log2FoldChangeIdx)
        l2FCnum = [];
    else
        l2FCnum = varargin{log2FoldChangeIdx+1};
    end
    
    if isempty(pIdx)
        p = [];
    else
        p = varargin{pIdx+1};
    end
    
    if isempty(violinIdx)
        violin = false;
    else
        violin = true;
    end
%%
uniqueGroups = unique(groups,'stable');

% making the plot labels
nTick = ceil(numel(uniqueGroups)/nComp);

if isempty(sz)
% size of the dots
    dotDensity = length(data)/length(uniqueGroups);
    if dotDensity< 1000
        sz = 10;
    elseif dotDensity > 1000
        sz = 0.5;
    end
end
thick = 2;

%select the stars according to p values
if not(isempty(p))
    for ip = 1:length(p)
        if p(ip) > 0.05
            stars{ip} = 'ns';
        elseif and(p(ip)>0.01,p(ip)<=0.05) 
            stars{ip} = '*';
        elseif and(p(ip)>0.001, p(ip)<=0.01) 
            stars{ip} = '**';
        elseif and(p(ip)>0.0001, p(ip)<=0.001)
            stars{ip} = '***';
        elseif p(ip)<=0.0001
            stars{ip} = '****';
        end
    end
end

if not(isempty(FCnum))
    for idx = 1:numel(FCnum)
        FC{idx} = ['FC=' num2str(FCnum(idx))];
    end
else
    FC = {};
end

if not(isempty(l2FCnum))
    for idx = 1:numel(l2FCnum)
        FC{idx} = ['log_{2}(f.c.)=' num2str(l2FCnum(idx))];
    end
end

for iTick = 1:nTick
    
    idxGr = ((iTick-1)*nComp) + (1:nComp);
    labelArray = uniqueGroups(idxGr);
    newline = repmat({'\\newline'},size(labelArray));
    newline(end) = {'\n'};
    tickLabels_tmp = cat(1,labelArray',newline');
    tickLabels{iTick} = strtrim(sprintf(cat(2,tickLabels_tmp{:})));
    
    dMixed = [];
    for idx = idxGr;
        g = uniqueGroups{idx};
        d = data(strcmp(groups,g));
        if iscolumn(d)
            dMixed = [dMixed d'];
        else
            dMixed = [dMixed d];
        end
    end
    
    x = sort(unique([linspace(min(dMixed),max(dMixed),1e4)]));

    for idx = idxGr;
        
        g = uniqueGroups{idx};
        d = data(strcmp(groups,g));
        n = sum(not(isnan(d)));
        thisColor = colors(idx,:);
        % percentiles
        C96(idx) = prctile(d,95.5);
        C4(idx) = prctile(d,4.5);
        
        [h] = ksdensity(d,x,'function','pdf');
        
        if violin
            h = 0.4*h/max(h);
        else
            h = 0.8*h/max(h);
        end
        
        [Ndots,~] = histcounts(d,round(n*std(d,'omitnan')/(C96(idx)-C4(idx))));
        
        if plotDots
            facnorm = 0.8*max(h)/max(Ndots-1);
            y=[];
            for idxh=1:length(Ndots)
                if violin
                    y = [y, 2*(facnorm*((1:Ndots(idxh))-1)-mean(facnorm*((1:Ndots(idxh))-1)))];
                else
                    y = [y, facnorm*((1:Ndots(idxh))-1)+0.1*max(h)];
                end
            end
            d(isnan(d)) = [];
            hold on
            plot(x,h + iTick,'color',thisColor,'LineWidth',thick)
            if violin
                plot(x,-h + iTick,'color',thisColor,'LineWidth',thick)
            end
            dots(idx) = scatter(sort(d),y+iTick,sz,thisColor,'filled');
            alpha(1)
            if violin 
                bv = -fliplr(h)+iTick;
            else
                bv = ones(1,length(h))*iTick;
            end
            fill([x fliplr(x)],[h+iTick bv],thisColor,'linestyle','none')
            alpha(.5)
        else
            hold on
            bv = ones(1,length(h))*iTick;
            plot(x,h + iTick,'color',thisColor,'LineWidth',thick)
            if violin
                plot(x,-h + iTick,'color',thisColor,'LineWidth',thick)
            end
            if violin 
                bv = -fliplr(h)+iTick;
            else
                bv = ones(1,length(h))*iTick;
            end
            fill([x fliplr(x)],[h+iTick bv],thisColor,'linestyle','none')
            alpha(.5)
        end

        [m(idx),imax] = max(h);
        xmax(rem(idx,2)+1) = x(imax);
    end
    if not(isempty(stars))
        line([xmax(1) xmax(1)],[1.1*m(1),1.2*m(1)]+iTick,'color','k','LineWidth',1)
        line([xmax(2) xmax(2)],[1.1*m(2),1.2*m(2)]+iTick,'color','k','LineWidth',1)
        line([xmax(1) xmax(2)],[1.15*m(2) 1.15*m(2)]+iTick,'color','k','LineWidth',1)
        text(mean([xmax(1) xmax(2)]),1.2*m(2)+iTick,stars(iTick),'HorizontalAlignment','center')
    end
    if not(isempty(FC))
        text(mean([xmax(1) xmax(2)]),1.1*m(2)+iTick,FC(iTick),'HorizontalAlignment','center')
    end
end
if violin
    yticks([1:nTick])
else
    yticks([1:nTick]+0.4)
end
yticklabels(tickLabels)
xlim([min(C4) max(C96)])
%legend
% colorDot(1) = scatter(0,1.5,sz,[1 0 0],'filled');
% colorDot(2) = scatter(0,1,sz,[0 0 1],'filled');
% colorDot(3) = scatter(0,1,sz,[0 0 0],'filled');
% alpha(colorDot,.5)
% legend(colorDot,{'Parental individual trajectory';'Isolated individual trajectory';'Random individual trajectory'})
