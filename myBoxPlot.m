function myBoxPlot(data,color,boxHeight,thick,idx,vertical)


med = median(data,'omitnan');
Q1 = prctile(data,25);
Q3 = prctile(data,75);
C5 = prctile(data,5);
C95 = prctile(data,95);

if vertical
    rectangle('Position',[idx-boxHeight,Q1,2*boxHeight,Q3-Q1],'EdgeColor',color,'LineWidth',thick,'Curvature',0.2)
    line([idx-boxHeight idx+boxHeight],[med med],'color',color,'LineWidth',thick)
    line([0 0]+idx,[C5 Q1],'LineWidth',thick,'color',color,'LineStyle','--')
    line([0 0]+idx,[Q3 C95],'LineWidth',thick,'color',color,'LineStyle','--')
    line([-boxHeight boxHeight]+idx,[C5 C5],'color',color,'LineWidth',thick)
    line([-boxHeight boxHeight]+idx,[C95 C95],'color',color,'LineWidth',thick)    
else 
    rectangle('Position',[Q1,idx-boxHeight,Q3-Q1,2*boxHeight],'EdgeColor',color,'LineWidth',thick,'Curvature',0.2)
    line([med med],[idx-boxHeight idx+boxHeight],'color',color,'LineWidth',thick)
    line([C5 Q1],[0 0]+idx,'LineWidth',thick,'color',color,'LineStyle','--')
    line([Q3 C95],[0 0]+idx,'LineWidth',thick,'color',color,'LineStyle','--')
    line([C5 C5],[-boxHeight boxHeight]+idx,'color',color,'LineWidth',thick)
    line([C95 C95],[-boxHeight boxHeight]+idx,'color',color,'LineWidth',thick)
end