function significantStars(stars,varargin)

if numel(varargin)>0
    verticalIdx = find(strcmp(varargin,'vertical'));
    
    if isempty(verticalIdx)
        vertical = false;
    else
        vertical = varargin{verticalIdx+1};
    end
else
    vertical = false;
end

if vertical
    line([4 6],[0 0],'color',[0 0 0])
    line([1 3],[0 0],'color',[0 0 0])
    line([2 5],[1 1],'color',[0 0 0])
    line([2 2],[0 1],'color',[0 0 0])
    line([5 5],[0 1],'color',[0 0 0])
    text(3.5,1.1,stars,'color',[0 0 0])
    ylim([-1,2])
    xlim([0 7])
else
    line([0 0],[4 6],'color',[0 0 0])
    line([0 0],[1 3],'color',[0 0 0])
    line([1 1],[2 5],'color',[0 0 0])
    line([0 1],[2 2],'color',[0 0 0])
    line([0 1],[5 5],'color',[0 0 0])
    text(1.1,3.5,stars,'color',[0 0 0],'Rotation',90,'verticalAlignment','bottom')
    xlim([-1,2])
    ylim([0 7])
end
xticklabels({})
yticklabels({})
axis off
end