function myBarPlot(data,groups,colors,varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% title : myBarPlot.m
% edition date : 2021-07-21
% description : beautiful bar plot.
% inputs : data = vector
%          groups = cell array containing to what group the data belongs to
%          colors = n x 3 matrix [RGB] for each group
% facultative inputs :
%           'dotSize' = area in mm^2 (50 by default)
%           'linewidth' = in mm (default 0.2)
%           'vertical' = true/false (false by default)
%           'showN' = true/false (true by default)
%           'legend' = true/false (true by default)
%           'compare' = n x 2 matrix that indicates what groups to compare
%           statistically
%           'p' = vector containing the p values of evert comparison
%           'stars' = cell array with the stars
%           'marker' = cell array for every group
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if numel(varargin)>0
    dotSizeIdx = find(strcmp(varargin,'dotSize'));
    linewidthIdx = find(strcmp(varargin,'linewidth'));
    verticalIdx = find(strcmp(varargin,'vertical'));
    nIdx = find(strcmp(varargin,'showN'));
    legendIdx = find(strcmp(varargin,'legend'));
    compIdx = find(strcmp(varargin,'compare'));
    pIdx = find(strcmp(varargin,'p'));
    starIdx = find(strcmp(varargin,'stars'));
    markerIdx = find(strcmp(varargin,'marker'));
else
    dotSizeIdx = [];
    linewidthIdx = [];
    verticalIdx = [];
    nIdx = [];
    legendIdx = [];
    compIdx = [];
    pIdx = [];
    starIdx = [];
    markerIdx = [];
end
    if isempty(verticalIdx)
        vertical = false;
    else
        vertical = varargin{verticalIdx+1};
    end
    
    if isempty(dotSizeIdx)
        sz = 50*(1/0.352778)^2; % convert to point, one point = 1/72 inch
    else
        sz = varargin{dotSizeIdx+1}*(1/0.352778)^2; % convert to point, one point = 1/72 inch
    end
    
    if isempty(linewidthIdx)
        thick = 0.2 *1/0.352778; % convert to point, one point = 1/72 inch
    else
        thick = varargin{linewidthIdx+1} *1/0.352778; % convert to point, one point = 1/72 inch
    end
    
    if isempty(nIdx)
        showN = true;
    else
        showN = varargin{nIdx+1};
    end
    
    if isempty(legendIdx)
        showLegend = true;
    else
        showLegend = varargin{legendIdx+1};
    end
    
    if isempty(compIdx)
        compare = [];
    else
        compare = varargin{compIdx+1};
    end
    
    if isempty(pIdx)
        p = [];
    else
        p = varargin{pIdx+1};
    end
    
    if isempty(starIdx)
        stars = [];
    else
        stars = varargin{starIdx+1};
    end
    
    if isempty(markerIdx)
        marker = [];
    else
        marker = varargin{markerIdx+1};
    end

%%

uniqueGroups = unique(groups,'stable');
dotDensity = length(data)/length(uniqueGroups);
boxThick = 1;
boxHeight = 0.4;

%select the stars according to p values
if not(isempty(p))
    for ip = 1:length(p)
        if p(ip) > 0.05
            stars{ip} = 'ns';
        elseif and(p(ip)>0.01,p(ip)<=0.05) 
            stars{ip} = '*';
        elseif and(p(ip)>0.001, p(ip)<=0.01) 
            stars{ip} = '**';
        elseif and(p(ip)>0.0001, p(ip)<=0.001)
            stars{ip} = '***';
        elseif p(ip)<=0.0001
            stars{ip} = '****';
        end
    end
end

for idx = 1:numel(uniqueGroups);
    
    thisColor = colors(idx,:);
    darkColor = 0.7*thisColor;
    g = uniqueGroups{idx};
    d = data(strcmp(groups,g));
    d(isnan(d)) = [];
    n = length(d);
    if isempty(marker)
        thisMarker = 'o';
    else
        thisMarker = marker{idx};
    end    
    
    % percentiles
    C96(idx) = prctile(d,95.5);
    C4(idx) = prctile(d,4.5);
    
    %the bar
    xbar = [-boxHeight  -boxHeight boxHeight boxHeight]+idx;
    ybar = [0 mean(d) mean(d) 0];
    thisError = std(d)/sqrt(n);
    
    %location of the dots
    x = sort(unique([linspace(min(d),max(d),1e4)]));
    [Ndots,~] = histcounts(d,round(sum(not(isnan(d)))/2));
    y = [];
    facnorm = 0.8*boxHeight/max(Ndots-1);
    for idxh=1:length(Ndots)
%             y = [y, 2*(facnorm*((1:Ndots(idxh))-1)-mean(facnorm*((1:Ndots(idxh))-1)))/Ndots(idxh)];
            y = [y, 2*(facnorm*((1:Ndots(idxh))-1)-mean(facnorm*((1:Ndots(idxh))-1)))];
    end
    
    hold on
    if vertical
       
        if showN
            text(boxHeight+0.1,median(d),['n = ' num2str(n)],'HorizontalAlignment','center')
        end
        % plot the data points
        dots(idx)=scatter(y+idx,sort(d),sz,darkColor,thisMarker,'filled');
        %the bar
        plot(xbar,ybar,'color',darkColor,'linewidth',thick)
        thisbar = fill(xbar,ybar,thisColor,'edgeColor',darkColor,'linewidth',thick);
        alpha(thisbar,.5)
        %the error
        line([0 0]+idx,[-1 1]*thisError+mean(d),'LineWidth',1,'color',thisColor,'LineStyle','-')
        line([-boxHeight boxHeight]/2+idx,[1 1]*thisError+mean(d),'color',thisColor,'LineWidth',1)
        line([-boxHeight boxHeight]/2+idx,-[1 1]*thisError+mean(d),'color',thisColor,'LineWidth',1) 
        %graph axes and ticks
%         ylim([min(C4) max(C96)])
        xticks([1:numel(uniqueGroups)])
        xticklabels(uniqueGroups)
    else
        %plot contours
        if showN
            text(median(d),boxHeight+0.1,['n = ' num2str(n)],'HorizontalAlignment','center')
        end
        % plot the data points
        dots(idx)=scatter(sort(d),y+idx,sz,darkColor,thisMarker,'filled');
        %the bar
        plot(ybar,xbar,'color',darkColor,'linewidth',thick)
        thisbar = fill(ybar,xbar,thisColor,'edgeColor',darkColor,'linewidth',thick);
        alpha(thisbar,.5)
        %the error
        line([-1 1]*thisError+mean(d),[0 0]+idx,'LineWidth',1,'color',thisColor)
        line([1 1]*thisError+mean(d),[-boxHeight boxHeight]/2+idx,'color',thisColor,'LineWidth',1)
        line(-[1 1]*thisError+mean(d),[-boxHeight boxHeight]/2+idx,'color',thisColor,'LineWidth',1) 
        %graph axes and ticks
%         xlim([0 1.1*max(C96)])
        yticks([1:numel(uniqueGroups)])
        yticklabels(uniqueGroups)
    end
    
    if showLegend
        blackDot = scatter(0,0,sz,[0 0 0],'filled');
        alpha(.5)
        legend(blackDot,'Individual cell')
    end
end

% significant stars
hold on



if not(isempty(compare))
    %sort data
    d = diff(compare');
    [d,iS] = sort(d);
    compare = compare(iS,:);
    stars = stars(iS);

    linePosVect = [];
    for iStar = 1:numel(stars)
        ix = compare(iStar,:);
        edges = [ix(1)+0.1 ix(2)-0.1];
        %lines
        linePos = max(C96([ix(1) ix(2)]))+0.1*max(C96);
        pointer1 = C96(ix(1))+0.05*max(C96);
        pointer2 = C96(ix(2))+0.05*max(C96);
        if and(sum(linePos<=linePosVect),d(iStar)>1)
           pointer1 = max(linePosVect(linePos<=linePosVect))+0.1*max(C96); 
           pointer2 = max(linePosVect(linePos<=linePosVect))+0.1*max(C96);
           linePos = max(linePosVect(linePos<=linePosVect))+0.15*max(C96);
        end
        
        if vertical
            line(edges,[1 1]*linePos,'color','k','LineWidth',1)
            line([1 1]*edges(1),[pointer1 linePos],'color','k','LineWidth',1)
            line([1 1]*edges(2),[pointer2 linePos],'color','k','LineWidth',1)
            text(mean(ix),linePos+0.05*max(C96),stars(iStar),'HorizontalAlignment','center')
        else
            line([1 1]*linePos,edges,'color','k','LineWidth',1)
            line([pointer1 linePos],[1 1]*edges(1),'color','k','LineWidth',1)
            line([pointer2 linePos],[1 1]*edges(2),'color','k','LineWidth',1)
            text(linePos+0.05*max(C96),mean(ix),stars(iStar),'HorizontalAlignment','left')
        end
            
        linePosVect(iStar) = linePos;
    end
end
    